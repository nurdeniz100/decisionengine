import { createTheme } from '@mui/material/styles';

export const appTheme = createTheme({
    palette: {
        type: 'light',
        primary: {
            main: '#945fa9',
            contrastText: '#fafafa',
        },
        secondary: {
            main: '#f2edfa',
        },
        background: {
            default: '#f2edfa',
        },
        text: {
            primary: '#2b0a57',
        },
    },
});