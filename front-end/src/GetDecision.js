import * as React from 'react';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import Link from '@mui/material/Link';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import {ThemeProvider} from '@mui/material/styles';
import axios from "axios";
import {appTheme} from "./AppTheme";
import {ReactComponent as InbankLogo } from "./inbankLogo.svg";
import {Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, SvgIcon} from "@mui/material";

function Copyright(props) {
    return (
        <Typography variant="body2" color="text.secondary" align="center" {...props}>
            {'Copyright © '}
            <Link color="inherit">
                Nur Deniz Çaylı Aras
            </Link>{' '}
            {new Date().getFullYear()}
            {'.'}
        </Typography>
    );
}

export default function GetDecision() {

    const [decisionMessage, setDecisionMessage] = React.useState("");
    const [periodMessage, setPeriodMessage] = React.useState("");
    const [amountMessage, setAmountMessage] = React.useState("");
    const [errorMessage, setErrorMessage] = React.useState("");
    const [errorTitle, setErrorTitle] = React.useState("");
    const [open, setOpen] = React.useState(false);

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const handleSubmit = (event) => {
        event.preventDefault();
        const data = new FormData(event.currentTarget);
        const decisionDto = JSON.stringify({
            personalCode: data.get('personalCode'),
            amount: data.get('amount'),
            period: data.get('period')
        });
        axios.post('http://localhost:8080/loan/decision/calculate', decisionDto, {
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(function (response) {
            if(response.status===200){
                setDecisionMessage(response.data.decision)
                setAmountMessage(response.data.amount)
                setPeriodMessage(response.data.period)
            }
        }).catch((error) => {
            if( error.response ){
                setErrorTitle(error.response.data.status)
                setErrorMessage(error.response.data.error)
                clearDecisionMessages()
                handleClickOpen()
            }
        })
    };

    const clearDecisionMessages = () => {
        setDecisionMessage("");
        setAmountMessage("");
        setPeriodMessage("");
    };

    return (
        <ThemeProvider theme={appTheme}>
            <Container component="main" maxWidth="xs">
                <CssBaseline/>
                <Box
                    sx={{
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'center',
                    }}
                >
                    <SvgIcon sx={{
                        width: 160,
                        height: 160,
                    }}>
                        <InbankLogo/>
                    </SvgIcon>
                    <Typography component="h1" variant="h5">
                        Inbank Loan Decision Application
                    </Typography>
                    <Box component="form" validate onSubmit={handleSubmit} sx={{mt: 1}}>
                        <TextField
                            margin="normal"
                            required
                            fullWidth
                            name="personalCode"
                            label="Personal Code"
                            InputProps={{ inputProps: { minLength: 11, maxLength: 11 }}}
                            id="personalCode"
                        />
                        <TextField
                            margin="normal"
                            required
                            fullWidth
                            id="amount"
                            label="Loan Amount"
                            type="number"
                            name="amount"
                            InputProps={{ inputProps: { min: 2000, max: 10000 } }}
                        />
                        <TextField
                            margin="normal"
                            required
                            fullWidth
                            name="period"
                            label="Loan Period"
                            type="number"
                            id="period"
                            InputProps={{ inputProps: { min: 12, max: 60 } }}
                        />
                        <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                            sx={{mt: 3, mb: 2}}
                        >
                            Get Decision
                        </Button>
                    </Box>
                </Box>
                <Box sx={{mt: 1, textAlign: "center"}}>
                    <Typography variant="body1">
                        Decision: {decisionMessage}
                    </Typography>
                    <Typography variant="body1">
                        Amount: {amountMessage}
                    </Typography>
                    <Typography variant="body1">
                        Period: {periodMessage}
                    </Typography>
                </Box>
                <Copyright sx={{mt: 8, mb: 4}}/>
            </Container>
            <Dialog
                open={open}
                onClose={handleClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">
                    Status: {errorTitle}
                </DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                        {errorMessage}
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose}>OK</Button>
                </DialogActions>
            </Dialog>
        </ThemeProvider>
    );
}