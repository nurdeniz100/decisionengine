package com.inbank.decisionengine.model;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class DecisionResponse {
    private String decision;
    private BigDecimal amount;
    private Integer period;

}
