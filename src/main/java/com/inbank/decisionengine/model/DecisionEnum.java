package com.inbank.decisionengine.model;

public enum DecisionEnum {
    MIN_AMOUNT(2000),
    MAX_AMOUNT(10000),
    MIN_PERIOD(12),
    MAX_PERIOD(60);

    DecisionEnum(Integer value) {
        this.value = value;
    }

    private final Integer value;

    public Integer getValue() {
        return value;
    }
}
