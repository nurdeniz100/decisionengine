package com.inbank.decisionengine.model;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class DecisionRequest {
    @NotNull
    private String personalCode;

    @Min(2000)
    @Max(10000)
    @NotNull
    private BigDecimal amount;

    @Min(12)
    @Max(60)
    @NotNull
    private Integer period;
}
