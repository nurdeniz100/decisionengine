package com.inbank.decisionengine.controller;

import com.inbank.decisionengine.model.DecisionRequest;
import com.inbank.decisionengine.model.DecisionResponse;
import com.inbank.decisionengine.service.DecisionService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("/loan/decision")
public class DecisionController {
    private final DecisionService decisionService;

    @PostMapping("/calculate")
    public ResponseEntity<DecisionResponse> calculate(@Valid @RequestBody DecisionRequest request){
        DecisionResponse response = decisionService.calculate(request);
        return ResponseEntity.ok(response);
    }
}
