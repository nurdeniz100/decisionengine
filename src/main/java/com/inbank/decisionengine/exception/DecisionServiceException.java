package com.inbank.decisionengine.exception;

public class DecisionServiceException extends RuntimeException {
    public DecisionServiceException(String message){
        super(message);
    }
}
