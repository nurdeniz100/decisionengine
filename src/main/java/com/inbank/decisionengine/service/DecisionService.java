package com.inbank.decisionengine.service;

import com.inbank.decisionengine.model.DecisionRequest;
import com.inbank.decisionengine.model.DecisionResponse;

public interface DecisionService {
    DecisionResponse calculate(DecisionRequest request);
}
