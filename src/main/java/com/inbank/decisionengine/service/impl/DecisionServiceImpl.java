package com.inbank.decisionengine.service.impl;

import com.inbank.decisionengine.model.DecisionRequest;
import com.inbank.decisionengine.model.DecisionResponse;
import com.inbank.decisionengine.service.DecisionService;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;

import static com.inbank.decisionengine.model.DecisionEnum.*;

@Service
public class DecisionServiceImpl implements DecisionService {
    @Override
    public DecisionResponse calculate(DecisionRequest request) {
        DecisionResponse response = new DecisionResponse();
        try{
            BigDecimal creditModifier = getCreditModifier(request.getPersonalCode());
            if(creditModifier.equals(BigDecimal.ZERO)){
                return createNegativeResponse("Person has debt.");
            }
            else if(creditModifier.compareTo(BigDecimal.valueOf(-1)) == 0){
                return createNegativeResponse("Person not found.");
            }

            BigDecimal creditScore = calculateCreditScore(creditModifier, request) ;
            BigDecimal calculatedAmount = creditScore.multiply(request.getAmount());
            Integer period = request.getPeriod();

            // If credit score greeter than or equals to 1, user can take more loan amount.
            if(creditScore.compareTo(BigDecimal.ONE) >= 0){
                calculatedAmount = calculatedAmount.compareTo(BigDecimal.valueOf(MAX_AMOUNT.getValue())) >= 1 ? BigDecimal.valueOf(MAX_AMOUNT.getValue()) : calculatedAmount;
            }
            // If credit score less than 1 and calculated amount is less than 2000, new period should be calculated.
            else if(creditScore.compareTo(BigDecimal.ONE) < 0  && calculatedAmount.compareTo(BigDecimal.valueOf(MIN_AMOUNT.getValue())) < 0) {
                period = request.getAmount().divide(creditModifier, 10, RoundingMode.HALF_UP).intValue();
                calculatedAmount = request.getAmount();

                // If period is greeter than 60, there is no valid period exists for loan.
                if(period > MAX_PERIOD.getValue()) {
                    return createNegativeResponse("Valid period not exists.");
                }
            }

            response.setDecision("Decision is positive!");
            response.setAmount(calculatedAmount);
            response.setPeriod(period);
        }
        catch(Exception e){
            response.setDecision(e.getMessage());
        }
        return response;
    }

    private DecisionResponse createNegativeResponse(String message) {
        DecisionResponse response = new DecisionResponse();
        response.setDecision(message + " Decision is negative!");
        response.setAmount(BigDecimal.ZERO);
        response.setPeriod(0);
        return response;
    }

    private BigDecimal calculateCreditScore(BigDecimal creditModifier, DecisionRequest request){
        return (creditModifier.divide(request.getAmount(), 10, RoundingMode.HALF_UP)).multiply(BigDecimal.valueOf(request.getPeriod()));
    }

    private BigDecimal getCreditModifier(String personalCode) {
        switch (personalCode) {
            case "49002010965" -> {
                return BigDecimal.ZERO;
            }
            case "49002010976" -> {
                return BigDecimal.valueOf(100);
            }
            case "49002010987" -> {
                return BigDecimal.valueOf(300);
            }
            case "49002010998" -> {
                return BigDecimal.valueOf(1000);
            }
            default -> {
                return BigDecimal.valueOf(-1);
            }
        }
    }
}
