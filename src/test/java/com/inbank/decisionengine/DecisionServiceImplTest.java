package com.inbank.decisionengine;

import com.inbank.decisionengine.model.DecisionRequest;
import com.inbank.decisionengine.model.DecisionResponse;
import com.inbank.decisionengine.service.impl.DecisionServiceImpl;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class DecisionServiceImplTest {
    @InjectMocks
    DecisionServiceImpl decisionService;

    DecisionRequest dummyRequest(String personalCode, BigDecimal amount, Integer period){
        DecisionRequest request = new DecisionRequest();
        request.setPersonalCode(personalCode);
        request.setAmount(amount);
        request.setPeriod(period);
        return  request;
    }

    @Test
    void calculate_WhenCreditModifierIsZero_ShouldReturnNegativeResponse() {
        DecisionRequest mockRequest = dummyRequest("49002010965", BigDecimal.valueOf(4000), 12);

        DecisionResponse response = decisionService.calculate(mockRequest);

        assertEquals("Person has debt. Decision is negative!", response.getDecision());
        assertEquals(BigDecimal.ZERO, response.getAmount());
        assertEquals(0, response.getPeriod());
    }

   @Test
    void calculate_WhenCreditModifierIsMinusOne_ShouldReturnNegativeResponse() {
        DecisionRequest mockRequest = dummyRequest("49002010974", BigDecimal.valueOf(4000), 12);

        DecisionResponse response = decisionService.calculate(mockRequest);

        assertEquals("Person not found. Decision is negative!", response.getDecision());
        assertEquals(BigDecimal.ZERO, response.getAmount());
        assertEquals(0, response.getPeriod());
    }

    @Test
    void calculate_WhenCreditScoreIsLessThanOne_ShouldCalculateAmountCorrectly() {
        DecisionRequest mockRequest = dummyRequest("49002010987", BigDecimal.valueOf(5000), 12);
        DecisionResponse response = decisionService.calculate(mockRequest);

        assertEquals("Decision is positive!", response.getDecision());
        assertEquals(3600, response.getAmount().intValue());
        assertEquals(12, response.getPeriod());
    }

    @Test
    void calculate_WhenCreditScoreIsGreaterThanOne_ShouldCalculateAmountCorrectly() {
        DecisionRequest mockRequest = dummyRequest("49002010998", BigDecimal.valueOf(4000), 12);

        DecisionResponse response = decisionService.calculate(mockRequest);

        assertEquals("Decision is positive!", response.getDecision());
        assertEquals(10000, response.getAmount().intValue());
        assertEquals(12, response.getPeriod());
    }

    @Test
    void calculate_WhenPeriodExceedsMaxPeriod_ShouldReturnNegativeResponse() {
        DecisionRequest mockRequest = dummyRequest("49002010976", BigDecimal.valueOf(10000), 12);
        DecisionResponse response = decisionService.calculate(mockRequest);

        assertEquals("Valid period not exists. Decision is negative!", response.getDecision());
        assertEquals(BigDecimal.ZERO, response.getAmount());
        assertEquals(0, response.getPeriod());
    }
}
