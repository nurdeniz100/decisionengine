# Loan Decision Engine

## Run Setup
- To run project in local:
    - Open a terminal and run this command: `docker-compose up`
    - After a time, back-end and front-end will be running up.
    - Front-end -> `http://localhost:3000`
    - Back-end -> `http://localhost:8080`

## Testing the API
- To test api:
    - Go the `http://localhost:8080/swagger-ui/index.html` for reaching Swagger UI.
    - Here you can test the API.

## Thought Process 
### CASE 1
  - If credit score is greater than 1 withing given loan amount and period, I used this formula: `creditScore * amount`
  - I compared result with `10000` and I give smaller one as result.

### CASE 2
- If credit score is less than 1 withing given loan amount and period, I used this formula: `creditScore * amount`
- I compared result with `2000` and if result is greater than `2000`, I accept this as result.
- If result is less than `2000`, I calculated new period for requested amount.
- If calculated period greater than `60`, no available period was found and decision is negative.

### CASE 3
- If customer has debt, decision is negative.



        

